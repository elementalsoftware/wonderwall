"use strict";

module.exports = function(io){
    io.origins("*:*");

    var state = {
        slideNo: 0,
        fragment: 0,
        nodeBlank: [],
        nodeMuted: [],
        autoTransition: false
    };

    var mediaState = [];
    var resetMediaState = function(){
        for(var i = 0; i < mediaState.length; i++){
            mediaState[i] = {
                playing: false
            };
        }
    };

    var autoSettings = {
        handle: null,
        timings: [],
        startTime: 0
    };

    var abortAuto = function(){
        if(autoSettings.handle){
            clearTimeout(autoSettings.handle);
            autoSettings.handle = null;
        }
        state.autoTransition = false;
        io.sockets.emit("state", state);
    };

    //call only from setTimeout
    var autoProgress = function(){
        autoSettings.handle = null;
        state.fragment++;

        if(state.fragment < autoSettings.timings.length){
            var nextTiming = new Date().getTime() - autoSettings.startTime;
            for(var i = 0; i < state.fragment; i++){
                nextTiming -= autoSettings.timings[i];
            }

            nextTiming = autoSettings.timings[state.fragment] - nextTiming;
            autoSettings.handle = setTimeout(autoProgress, nextTiming);
        }else{
            state.autoTransition = false;
        }
        io.sockets.emit("state", state);
    };

    io.on("connection", function (socket) {
        socket.emit("state", state);
        //Not yet implemented client-side
        /*socket.emit("mediaState", {
            timeNow: new Date().getTime(),
            mediaState: mediaState
        });*/
        socket.on("state_update", function (data) {
            if(data.slideNo !== state.slideNo){
                resetMediaState();
            }
            state = data;

            socket.broadcast.emit("state", state);
        });
        socket.on("media_play", function(nodeList){
            if(nodeList instanceof Array){
                nodeList.forEach(function(nodeId){
                    var timeOfs = 0;
                    if(mediaState.hasOwnProperty(nodeId) && mediaState[nodeId].hasOwnProperty("pauseTime")){
                        timeOfs = mediaState[nodeId].pauseTime;
                    }
                    mediaState[nodeId] = {
                        playing: true,
                        startTime: new Date().getTime() - timeOfs
                    };
                });
                io.sockets.emit("media_play", nodeList);
            }
        });
        socket.on("media_pause", function(nodes){
            for(var nodeId in nodes){
                if(nodes.hasOwnProperty(nodeId)){
                    mediaState[nodeId] = {
                        playing: false,
                        pauseTime: nodes[nodeId]
                    };
                }
            }
            io.sockets.emit("media_pause", nodes);
        });
        socket.on("start_auto", function(timings){
            if(state.autoTransition || state.fragment >= timings.length){
                socket.emit("state", state);
                return;
            }
            autoSettings.timings = timings;
            autoSettings.startTime = new Date().getTime();
            for(var i = 0; i < state.fragment; i++){
                autoSettings.startTime -= timings[i];
            }
            var nextTiming = timings[state.fragment];
            autoSettings.handle = setTimeout(autoProgress, nextTiming);
            state.autoTransition = true;
            io.sockets.emit("state", state);
        });
        socket.on("stop_auto", function(){
            if(!state.autoTransition){
                return;
            }
            abortAuto();
        });
    });
};
