"use strict";

var express = require("express"),
	app = express(),
	server = require("http").Server(app),
	io = require("socket.io")(server),
	appConfig = require(__dirname + "/config.js");

app.use(express.static(appConfig.webserver.staticDir));

var sioServer = "127.0.0.1:" + appConfig.webserver.port;
var argv = require("yargs").argv;
if(argv.hasOwnProperty("master")){
	sioServer = argv.master;
}else if(argv.hasOwnProperty("slave")){
	sioServer = argv.slave;
}else{
	var interfaces = require("os").networkInterfaces();
	var found = false;
	for(var ifc in interfaces){
		if(interfaces.hasOwnProperty(ifc)){
			var ifcObj = interfaces[ifc];
			for(var idx in ifcObj){
				if(ifcObj.hasOwnProperty(idx) && !ifcObj[idx].internal && ifcObj[idx].family === "IPv4"){
					sioServer = ifcObj[idx].address + ":" + appConfig.webserver.port;
					found = true;
					break;
				}
			}
			if(found){
				break;
			}
		}
	}
}
if(!argv.hasOwnProperty("slave")){
	require("./backend/socketio.js")(io);
}

app.get("/sync-config", function(req, res){
	res.json({server:sioServer});
});

server.listen(appConfig.webserver.port);
