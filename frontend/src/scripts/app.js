(function(){
	"use strict";

	angular.element(document).ready(function() {
		var initInjector = angular.injector(["ng"]);
		var $http = initInjector.get("$http");
		$http.get("/sync-config").then(function (response) {
			var config = response.data;
			angular.module("pillars").constant("SocketConfig", config);
			angular.bootstrap(document, ["pillars"]);
		});
	});

	angular.module("pillars", [
		"pillars.controllers",
		"pillars.filters",
		"pillars.services",
		"pillars.directives",
		"ngRoute",
		"ngMdIcons",
		"ngMaterial"
	])
	.constant("AppProperties", {
		name: "WonderWall"
	})
	.config(["$routeProvider", "$mdThemingProvider", function($routeProvider, $mdThemingProvider){
		$mdThemingProvider.definePalette("custom_teal", {
			"50": "#e0f2f1",
		    "100": "#b2dfdb",
		    "200": "#80cbc4",
		    "300": "#4db6ac",
		    "400": "#26a69a",
		    "500": "#009688",
		    "600": "#00897b",
		    "700": "#00796b",
		    "800": "#00695c",
		    "900": "#004d40",
		    "A100": "#a7ffeb",
		    "A200": "#64ffda",
		    "A400": "#1de9b6",
		    "A700": "#00bfa5",
		    "contrastDefaultColor": "dark",
		    "contrastLightColors": "500 600 700 800 900",
		    "contrastStrongLightColors": "500 600 700"
		});
		$mdThemingProvider.theme("default")
			.dark(true)
			.primaryPalette("custom_teal");

		$routeProvider
			.when("/", {
				controller: "NodeSelectController",
				templateUrl: "views/node_select.html",
				controllerAs: "nodeSelectCtrl"
			})
			.when("/view/:nodeId", {
				controller: "DeckController",
				templateUrl: "views/deck.html",
				controllerAs: "deckCtrl"
			})
			.when("/control", {
				controller: "ControlController",
				templateUrl: "views/control.html",
				controllerAs: "ctrlCtrl"
			})
			.otherwise({
				redirectTo: "/"
			});

	}]);
})();
