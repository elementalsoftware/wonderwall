#Wonderwall

##Slide configuration
Each slide may have the following attributes:

###`content` (String | Array[String])
The HTML for the slide. If an array is provided its contents will be concatenated.

###`layout` (Object)
Specifies the alignment of the slide contents. The object may have `v` and `h` attributes which each may contain one of the values 'start', 'end' or 'center'.

###`notes` (String | Array[String]) TODO
The HTML for the speaker notes for the slide

###`fragments` (Integer)
Specifies how many slide fragments this slide comprises. Individual fragment portions should be tagged with the HTML attribute slide-fragment and may also have one or more of the following HTML attributes:

####`fragment-show` (Integer)
The fragment index to begin showing the fragment (0-based)

####`fragment-hide` (Integer)
The fragment index to stop showing the fragment (0-based)

####`fragment-mode` (String)
This attribute value may be either 'display' or 'visibility'. If omitted it is assumed to be 'visibility'. Represents the mechanism used for hiding the element (visibility continues to consume space while display completely removes the element).

####Fragment Example
```html
<div slide-fragment fragment-hide="3" fragment-mode="display">I'll disappear at fragment 3 and the rest will shift up</div>
<div slide-fragment fragment-hide="4">I'll disappear at fragment 4</div>
<div slide-fragment fragment-show="2">I'll appear at fragment 2, but I'll take up space until then</div>
<div slide-fragment fragment-hide="3" fragment-show="1">I'll appear at 1 and disappear at 3</div>
```

###`nodes` (Array[Object])
Node-specific configuration for this slide. The object MUST contain an `id` attribute. The object may contain the following values which will override the attribute value from the slide in general (i.e. if you provide a slide content and a node content for one node, the other nodes will use the slide content while the customised one will use the specified node-specific content instead). Note that a node may only have one entry in this array per slide. Additional entries may be ignored.

####`id` (Integer | Array[Integer]) REQUIRED TODO: Support array syntax
The ID(s) of the nodes to which this specific configuration applied.

####`content` (String | Array[String])
See Slide configuration.

###`video` (String) TODO
Path to video file to use as this slide. Note that if video is specified then `content` will be ignored.

###`animate` (Object)
The animate object may contain an `in` and/or `out` object each describing the associated animation configuration for slide becoming visible or being hidden respectively.
Each animation object must contain a `type` attribute (see list below for valid types), and a `duration` attribute which is the time in milliseconds.
Depending on the selected animation type, additional arguments may be required.

The following list describes the valid animation types (and the associated arguments):

* `slide-out-right`
* `spin-zoom-in`

###`autoAdvance` TODO
