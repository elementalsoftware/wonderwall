(function(){
	"use strict";

	angular.module("pillars.directives", [
        "pillars.services"
    ])
	.directive("wwSlide", ["$rootScope", "DeckConfigService", "$sce", "$compile", function($rootScope, DeckConfigService, $sce, $compile){
        return {
            priority: 1000,
            restrict: "E",
            scope: {
                displaySlide: "@",
				enableVideo: "@", //non-reactive
                nodeId: "@",
				animationInProgress: "=",

				//The rest of these attributes are delgated to sub-directives

				//For use by fragment-manager
				displayFragment: "@",

				//For use by slide-video
				muteAudio: "@",
				videoPlaying: "@", //Also used by this directive
				videoPauseTime: "@",
				onVideoEnd: "&"
            },

            link: function(scope, elem/*, attrs*/){
                scope.slideContent = "";
				var animInTimeout = null;
				var animInClass = "";
                var renderSlide = function(){

					var slideConfig = {};
					var slideNodeConfig = {};
					var deckConfig = {};
                    if(DeckConfigService.config){
                        slideConfig = DeckConfigService.config.slides[scope.displaySlide] || {};
                        deckConfig = DeckConfigService.config.deck || {};
					}

                    if(slideConfig.hasOwnProperty("nodes")){
                        slideConfig.nodes.forEach(function(node){
                            if(node.id === Number(scope.nodeId)){
                                slideNodeConfig = node;
                            }
                        });
                    }

                    var slideContent = slideNodeConfig.content || slideConfig.content || "";
					scope.videoMode = false;
					if(slideConfig.hasOwnProperty("video") || slideNodeConfig.hasOwnProperty("video")){
						if(scope.enableVideo){
							var videoUrl = slideNodeConfig.video || slideConfig.video;
							var subtitleFile = slideNodeConfig.subtitles || slideConfig.subtitles || "";
							if(subtitleFile){
								subtitleFile = "<track kind=\"captions\" src=\"" + subtitleFile + "\" default>";
							}
							slideContent = "<video slide-video video-playing=\"{{videoPlaying}}\" video-pause-time=\"{{videoPauseTime}}\" on-video-end=\"onVideoEnd()\" mute-audio=\"{{muteAudio}}\" src=\"" + videoUrl + "\" preload=\"auto\">" + subtitleFile + "</video>";
						}else{
							slideContent = "<h1 class=\"video-placeholder\">&lt;video&gt;</h1><h1 class=\"video-playing-indicator\"><ng-md-icon icon=\"play_arrow\" ng-show=\"videoPlaying == 'true'\"></ng-md-icon><ng-md-icon icon=\"pause\" ng-hide=\"videoPlaying == 'true'\"></ng-md-icon></h1>";
							//todo: add playing-state icon
						}
						scope.videoMode = true;
					}

					//Content align
					var nodeLayout = slideNodeConfig.layout || {};
					var layout = slideConfig.layout || {};
					var deckLayout = deckConfig.layout || {};
					scope.layout = (nodeLayout.h || layout.h || deckLayout.h || "center") + " " + (nodeLayout.v || layout.v || deckLayout.v || "center");
					if(scope.videoMode){
						//For thumbnails
						scope.layout = "center center";
					}

					//Entry animation
					var anim = {};
					var nodeAnim = {};
					if(slideConfig.animate && slideConfig.animate.hasOwnProperty("in")){
						anim = slideConfig.animate.in;
					}
					if(slideNodeConfig.animate && slideNodeConfig.animate.hasOwnProperty("in")){
						nodeAnim = slideNodeConfig.animate.in;
					}
					//Duration MUST be slide-global, not node-specific
					var animDuration = 0;
					animInClass = "";
					if(anim.duration){
						animDuration = anim.duration;
						var animType = nodeAnim.type || anim.type || "";
						if(animType !== ""){
							animInClass = "animate-" + animType;
						}
					}

					//Background/foreground
					var slideBg = slideNodeConfig.background || slideConfig.background || deckConfig.background || "";
					var slideFg = slideNodeConfig.foreground || slideConfig.foreground || deckConfig.foreground || "";
					elem.css("background", slideBg);
					elem.css("color", slideFg).css("fill", slideFg);

					var fontScale = slideNodeConfig.fontScale || slideConfig.fontScale || deckConfig.fontScale || "";
					if(fontScale !== ""){
						fontScale = "font-scale=\"" + fontScale + "\"";
					}

					if(slideContent instanceof Array){
						//Content may be an array, for readability purposes in the json
						//just join it all up
						slideContent = slideContent.join("");
					}

					var disableMargin = "";
					if(slideNodeConfig.disableMargin || slideConfig.disableMargin || deckConfig.disableMargin){
						disableMargin = "disable-margin";
					}

                    if(scope.slideContent !== slideContent){
						scope.slideContent = slideContent; //To track when stuff actually changes
						elem.empty().append(angular.element($compile("<div class=\"slide-content " + animInClass + " " + disableMargin + "\" style=\"-webkit-animation-duration: " + animDuration + "ms;" + slideBg + slideFg + "\" ng-class=\"{'video-slide': videoMode}\" flowtype " + fontScale + " fragment-manager display-fragment=\"{{displayFragment}}\" layout=\"column\" layout-align=\"{{layout}}\">"+slideContent+"</div>")(scope)));
						if(animInTimeout !== null){
							clearTimeout(animInTimeout);
						}
						animInTimeout = setTimeout(function(){
							animInTimeout = null;
							elem.children(".slide-content").removeClass(animInClass);
						}, animDuration);
                    }
                };

				var displayedSlide = null;
				var animationInProgress = null;
				var abortAnimations = function(){
					var target = elem.children(".slide-content");
					if(animInTimeout !== null){
						//Abort animation
						clearTimeout(animInTimeout);
						animInTimeout = null;
						target.removeClass(animInClass);
						renderSlide();
						return;
					}
					if(animationInProgress){
						//Abort animation
						clearTimeout(animationInProgress);
						animationInProgress = null;
						renderSlide();
						return;
					}
				};

				var updateContent = function(){
					//Exit animation (entry handled in renderSlide)
					if(displayedSlide !== null && scope.displaySlide !== displayedSlide){
						abortAnimations();
						var target = elem.children(".slide-content");
						var anim = DeckConfigService.config.slides[displayedSlide].animate;
						if(anim && anim.hasOwnProperty("out")){

							animationInProgress = setTimeout(function(){
								animationInProgress = null;
								renderSlide();
							}, anim.out.duration);
							target.css("-webkit-animation-duration",anim.out.duration+"ms").addClass("animate-" + anim.out.type);
						}else{
							renderSlide();
						}
					}else{
						renderSlide();
					}
					displayedSlide = scope.displaySlide;
				};
                scope.$watch("displaySlide", updateContent);
                scope.$watch("nodeId", updateContent);
                $rootScope.$on("DeckAvailable", updateContent);
            }
        };
    }])
	.directive("slideVideo", function(){
		return {
			priority: 900,
			restrict: "A",
			//require: "^wwSlide",
			scope: {
				muteAudio: "@",
				videoPlaying: "@",
				videoPauseTime: "@",
				onVideoEnd: "&"
			},
			link: function(scope, element/*, attrs*/){
				//var endedFired = false;
				element.on("$destroy", function(){
					//Must destroy scope to prevent odd issues
					scope.$destroy();
				});

				element.on("ended", function(){
					//endedFired = true;
					scope.$apply(function(){
						scope.onVideoEnd();
					});
				});

				scope.$watch("videoPlaying", function(newVal, oldVal){
					newVal = (newVal === "true");
					oldVal = (oldVal === "true");

					if(oldVal && !newVal/* && !endedFired*/){
						element[0].pause();
						element[0].currentTime = Number(scope.videoPauseTime) / 1000;
						if(Number(scope.videoPauseTime) === 0){
							element.addClass("video-hidden");
						}
					}
					if(newVal && !oldVal){
						element.removeClass("video-hidden");
						element[0].play();
						//endedFired = false;
					}
				});
				scope.$watch("muteAudio", function(newVal){
					element.prop("muted", (newVal === "true"));
				});
				if(scope.muteAudio === "true"){
					element.attr("muted", "true");
					element.prop("muted", true);
				}
				element.addClass("video-hidden");
			}
		};
	})
	.directive("flowtype", function(){
		return {
            priority: 0,
			restrict: "A",
			link: function(scope, elem, attrs){
				setTimeout(function(){
					$(elem).flowtype({
						minFont: 1,
						maxFont: 9999,
						minimum: 1,
						maximum: 9999,
						fontRatio: attrs.fontScale || 25
					});
				});
			}
		};
	})
	.directive("fragmentManager", [function(){
		return {
            priority: 900,
            restrict: "A",
            scope: {
				displayFragment: "@"
			},
			controller: ["$scope", function($scope){
				var processFragment = function(fragmentConfig, newFragment){
					fragmentConfig.visibility.visible = (newFragment >= fragmentConfig.show && newFragment < fragmentConfig.hide);
				};

				var fragments = [];
				$scope.$watch("displayFragment", function(newFragment){
					newFragment = Number(newFragment);
					fragments.forEach(function(fragment){ processFragment(fragment, newFragment); });
				});
				this.registerFragment = function(show, hide, visibility){
					var fragmentObj = {
						show: Number(show || 0),
						hide: Number(hide || Math.min()), //Don't ask
						visibility: visibility
					};
					fragments.push(fragmentObj);
					processFragment(fragmentObj, Number($scope.displayFragment));
					return function(){
						var idx = fragments.indexOf(fragmentObj);
						if(idx !== -1){
							fragments.splice(idx, 1);
						}
					};
				};
			}],
		};
	}])
	.directive("slideFragment", [function(){
		return {
            priority: 900,
            restrict: "A",
			require: "^fragmentManager",
            scope: {
				fragmentShow: "@",
				fragmentHide: "@",
				fragmentMode: "@"
			},
			link: function(scope, element, attrs, manager){
				scope.visibility = {
					visible: true
				};
				scope.$watch("visibility.visible", function(newValue){
					var className = "hidden-fragment-visibility";
					if(scope.fragmentMode === "display"){
						className = "hidden-fragment-display";
					}else if(scope.fragmentMode === "opacity"){
						className = "hidden-fragment-opacity";
					}
					if(newValue){
						element.removeClass(className);
					}
					if(!newValue){
						element.addClass(className);
					}
				});
				var remover = manager.registerFragment(scope.fragmentShow, scope.fragmentHide, scope.visibility);
				scope.$on("$destroy", function(){
					remover();
				});
			}
		};
	}]);
}());
