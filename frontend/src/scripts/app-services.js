(function(){
	"use strict";
	angular.module("pillars.services", [
		'btford.socket-io'
	])
	.factory("DeckConfigService", ["$http", "$rootScope", function($http, $rootScope){
        var svc = {
            config: null
        };

        $http.get("/slides.json")
            .success(function(data){
                svc.config = data;
				$rootScope.$broadcast("DeckAvailable");
            });

        return svc;
    }])
	.factory("Socket", ["socketFactory", "SocketConfig", function(socketFactory, SocketConfig){
		var socket = io.connect("http://" + SocketConfig.server);

		return socketFactory({
			ioSocket: socket
		});
	}]);
}());
