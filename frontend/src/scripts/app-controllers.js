(function(){
	"use strict";

	angular.module("pillars.controllers", [])
	.controller("MainController", ["AppProperties", "DeckConfigService", "$rootScope", "Socket", function(AppProperties, DeckConfigService, $rootScope, Socket){
		var self = this;
		self.properties = AppProperties;
		self.deck = null;
		$rootScope.$on("DeckAvailable", function(){
			self.deck = DeckConfigService.config;
			self.resetMediaState();
		});
		$rootScope.$on("SlideChanged", function(){
			self.resetMediaState();
		});
		self.fullScreen = function(){
			document.getElementsByTagName("body")[0].webkitRequestFullscreen();
		};
		self.getNodeById = function(id){
			for(var i=0; i<=self.deck.nodes.length; i++){
				var node = self.deck.nodes[i];
				if(node.id === id){
					return node;
				}
			}
		};
		self.deckState = {
	        slideNo: 0,
	        fragment: 0,
	        nodeBlank: [],
			nodeMuted: [],
			autoTransition: false
	    };
		self.mediaState = [];
		Socket.on("state", function(newState){
			var oldState = self.deckState;
			self.deckState = newState;
			if(!oldState || oldState.slideNo !== newState.slideNo){
				$rootScope.$broadcast("SlideChanged");
			}
		});
		Socket.on("media_play", function(nodes){
			nodes.forEach(function(node){
				var timeOfs = 0;
				if(self.mediaState.hasOwnProperty(node) && self.mediaState[node].hasOwnProperty("pauseTime")){
					timeOfs = self.mediaState[node].pauseTime;
				}
				self.mediaState[node] = {
					playing: true,
					startTime: new Date().getTime() - timeOfs
				};
			});
		});
		Socket.on("media_pause", function(nodes){
			for(var node in nodes){
				if(nodes.hasOwnProperty(node)){
					self.mediaState[node] = {
						playing: false,
						pauseTime: nodes[node]
					};
				}
			}
		});
		var sendState = function(){
			Socket.emit("state_update", self.deckState);
		};
		self.resetMediaState = function(){
			if(self.deck){
				self.deck.nodes.forEach(function(nodeConfig){
					self.mediaState[nodeConfig.id] = {playing: false};
				});
			}
		};

		self.getNumFragments = function(slideNo){
			return self.deck.slides[slideNo].fragments || 1;
		};
		self.prevSlide = function(){
			if(self.hasFragment(self.deckState.slideNo)){
				if(self.deckState.fragment > 0){
					self.deckState.fragment--;
				}else if(self.deckState.slideNo > 0){
					self.deckState.slideNo--;
					self.deckState.fragment = self.getNumFragments(self.deckState.slideNo) - 1;
					$rootScope.$broadcast("SlideChanged");
				}
			}else{
				self.deckState.slideNo = Math.max(0, self.deckState.slideNo - 1);
				self.deckState.fragment = self.getNumFragments(self.deckState.slideNo) - 1;
				$rootScope.$broadcast("SlideChanged");
			}
			sendState();
		};
		self.nextSlide = function(){
			if(self.hasFragment(self.deckState.slideNo)){
				if((self.deckState.fragment + 1) < self.getNumFragments(self.deckState.slideNo)){
					self.deckState.fragment++;
				}else if((self.deckState.slideNo + 1) < self.deck.slides.length){
					self.deckState.fragment = 0;
					self.deckState.slideNo++;
					$rootScope.$broadcast("SlideChanged");
				}
			}else{
				self.deckState.slideNo = Math.min(self.deck.slides.length - 1, self.deckState.slideNo + 1);
				self.deckState.fragment = 0;
				$rootScope.$broadcast("SlideChanged");
			}
			sendState();
		};
		self.hasFragment = function(slideNo){
			if(!self.deck){
				return false;
			}
			var slide = self.deck.slides[slideNo];
			return Boolean(slide && slide.hasOwnProperty("fragments") && slide.fragments > 1);
		};
		self.hasFragmentTimings = function(slideNo){
			if(!self.deck){
				return false;
			}
			var slide = self.deck.slides[slideNo];
			return Boolean(slide && slide.hasOwnProperty("fragmentTimings"));
		};
		self.getNode = function(nodeId){
			nodeId = Number(nodeId);
			var node = null;
			self.deck.nodes.forEach(function(nodeConfig){
				if(nodeConfig.id === nodeId){
					node = nodeConfig;
				}
			});
			return node;
		};
		self.toggleNodeBlank = function(nodeId, state){
			var node = self.getNode(nodeId);
			if(node){
				self.deckState.nodeBlank[nodeId] = state;
				sendState();
			}
		};
		self.toggleBlank = function(state){
			self.deck.nodes.forEach(function(node){
				self.toggleNodeBlank(node.id, state);
			});
			sendState();
		};
		self.anyNodesBlank = function(){
			var anyBlank = false;
			self.deck.nodes.forEach(function(node){
				if(self.deckState.nodeBlank[node.id]){
					anyBlank = true;
				}
			});
			return anyBlank;
		};
		self.toggleNodeMute = function(nodeId, state){
			var node = self.getNode(nodeId);
			if(node){
				self.deckState.nodeMuted[nodeId] = state;
				sendState();
			}
		};
		self.toggleMute = function(state){
			self.deck.nodes.forEach(function(node){
				self.toggleNodeMute(node.id, state);
			});
			sendState();
		};
		self.anyNodesMuted = function(){
			var anyMuted = false;
			self.deck.nodes.forEach(function(node){
				if(self.deckState.nodeMuted[node.id]){
					anyMuted = true;
				}
			});
			return anyMuted;
		};
		self.mediaPlay = function(){
			var nodes = [];
			for(var idx in self.deck.nodes){
				if(self.deck.nodes.hasOwnProperty(idx)){
					nodes.push(self.deck.nodes[idx].id);
				}
			}
			Socket.emit("media_play", nodes);
		};
		self.mediaPause = function(){
			var nodes = {};
			var node;
			var timeNow = new Date().getTime();
			for(var idx in self.deck.nodes){
				if(self.deck.nodes.hasOwnProperty(idx)){
					node = self.deck.nodes[idx];
					if(self.mediaState.hasOwnProperty(node.id) && self.mediaState[node.id].playing && self.mediaState[node.id].hasOwnProperty("startTime")){
						nodes[node.id] = timeNow - self.mediaState[node.id].startTime;
					}
				}
			}
			Socket.emit("media_pause", nodes);
		};
		self.mediaEnded = function(nodeIdx){
			if(self.mediaState[nodeIdx].playing){
				self.mediaState[nodeIdx] = {
					playing: false,
					pauseTime: 0
				};
				var sioObj = {};
				sioObj[nodeIdx] = 0;
				Socket.emit("media_pause", sioObj);
			}
		};
		self.autoTransitionStart = function(){
			if(self.deckState.autoTransition){
				return;
			}
			var slide = self.deck.slides[self.deckState.slideNo];
			if(!slide.hasOwnProperty("fragmentTimings")){
				return;
			}
			self.deckState.autoTransition = true;
			Socket.emit("start_auto", slide.fragmentTimings);
		};
		self.autoTransitionStop = function(){
			if(!self.deckState.autoTransition){
				return;
			}
			Socket.emit("stop_auto");
		};
	}])
	.controller("NodeSelectController", [function(){
		//var self = this;
		//self.configSvc = DeckConfigService;
	}])
	.controller("DeckController", ["$routeParams", function($routeParams){
		var self = this;
		self.nodeId = Number($routeParams.nodeId);
	}])
	.controller("ControlController", [function(){
		var self = this;
		self.viewingNode = 1;
		self.muted = true;
	}]);
}());
